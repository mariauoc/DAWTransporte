/*
 *  Herencia: Alquiler de vehículos
 */
package transporte;

import excepciones.ExceptionTransporte;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Camion;
import modelo.Coche;
import modelo.Furgoneta;
import modelo.Microbus;
import modelo.Vehiculo;

/**
 *
 * @author DAM
 */
public class Transporte {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Furgoneta f = new Furgoneta(true, 2500, "1234AAA");
            Camion ca = new Camion(8000, "5678BBB");
            Coche co = new Coche(5, "9999CCC");
            Microbus b = new Microbus(25, "6666DDD");
            ArrayList<Vehiculo> flota = new ArrayList<>();
            // Añadimos los vehiculos al arraylist
            flota.add(f);
            flota.add(ca);
            flota.add(co);
            flota.add(b);
            // Mostramos matrícula y alquiler para 8 días de cada coche
            System.out.println("Listado de vehículos y precio alquiler 8 días");
            for (Vehiculo v : flota) {
                System.out.println("Matricula "+v.getMatricula()+ " Alquiler: "+v.calcularAlquiler(8));
            }
            System.out.println("Todos los datos de cada Vehiculo");
            for (Vehiculo v : flota) {
                System.out.println("Vehículo con matrícula: "+ v.getMatricula());
                if (v instanceof Furgoneta) {
                    Furgoneta aux = (Furgoneta) v;
                    System.out.println("Es una Furgoneta");
                    System.out.println("Refrigeracion "+aux.isRefrigeracion());
                    System.out.println("PMA: "+aux.getPma());
                }
            }
        } catch (ExceptionTransporte ex) {
            System.out.println(ex.getMessage());
        }
    }
    
}
