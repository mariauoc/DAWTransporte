/*
 * Excepción propia para tratar errores de datos en el modelo de Transporte
 */
package excepciones;

/**
 *
 * @author DAM
 */
public class ExceptionTransporte extends Exception {

    public ExceptionTransporte(String message) {
        super(message);
    }
    
}
