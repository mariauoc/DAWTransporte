/*
 * Coche hereda de VehiculoPersona
 */
package modelo;

import excepciones.ExceptionTransporte;

/**
 *
 * @author DAM
 */
public class Coche extends VehiculoPersona {

    public Coche(int plazas, String matricula) throws ExceptionTransporte {
        super(plazas, matricula);
    }

    @Override
    public double calcularAlquiler(int dias) {
        double alquiler = super.calcularAlquiler(dias) + (1.5 * getPlazas() * dias);
        return alquiler;
    }
    
}
