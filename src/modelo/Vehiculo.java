/*
 * Clase Vehículo
 */
package modelo;

import excepciones.ExceptionTransporte;

/**
 *
 * @author DAM
 */
public abstract class Vehiculo {

    private String matricula;

    public Vehiculo(String matricula) throws ExceptionTransporte {
        if (matricula.length() == 7) {
            this.matricula = matricula;
        } else {
            throw new ExceptionTransporte("ERROR: Matrícula incorrecta");
        }
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) throws ExceptionTransporte {
        if (matricula.length() == 7) {
        this.matricula = matricula;
        } else {
            throw new ExceptionTransporte("ERROR: Matrícula incorrecta");
        }
    }

    public double calcularAlquiler(int dias) {
        return 50 * dias;
    }

}
