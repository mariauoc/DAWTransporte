/*
 * Microbus hereda de VehiculoPersona
 */
package modelo;

import excepciones.ExceptionTransporte;

/**
 *
 * @author DAM
 */
public class Microbus extends VehiculoPersona {

    public Microbus(int plazas, String matricula) throws ExceptionTransporte {
        super(plazas, matricula);
    }

    @Override
    public double calcularAlquiler(int dias) {
        return super.calcularAlquiler(dias) + 2 * getPlazas();
    }

}
