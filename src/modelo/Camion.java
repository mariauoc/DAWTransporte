/*
 * Camión hereda de VehiculoCarga
 */
package modelo;

import excepciones.ExceptionTransporte;

/**
 *
 * @author DAM
 */
public class Camion extends VehiculoCarga {

    public Camion(int pma, String matricula) throws ExceptionTransporte {
        super(pma, matricula);
    }

    @Override
    public double calcularAlquiler(int dias) {
        return super.calcularAlquiler(dias) + 40;
    }
    
}
