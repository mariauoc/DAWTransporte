/*
 * Vehiculo Carga hereda de Vehiculo
 */
package modelo;

import excepciones.ExceptionTransporte;

/**
 *
 * @author DAM
 */
public abstract class VehiculoCarga extends Vehiculo {
    
    private int pma;

    public VehiculoCarga(int pma, String matricula) throws ExceptionTransporte {
        super(matricula);       // Llama al constructor padre (Vehiculo)
        this.pma = pma;
    }

    public int getPma() {
        return pma;
    }

    public void setPma(int pma) {
        this.pma = pma;
    }

    @Override
    public double calcularAlquiler(int dias) {
        return super.calcularAlquiler(dias) + (20 * pma);
    }
    
}
